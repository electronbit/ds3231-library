#include <DS323x.h>

DS323x rtc;

void setup()
{
    Serial.begin(9600);
    Wire.begin();
    delay(1000);

    rtc.attach(Wire);
    rtc.now(DateTime(2020, 7, 5, 13, 30, 31));
}

void loop()
{
    DateTime now = rtc.now();
    Serial.println(now.timestamp());
    delay(2000);
}
